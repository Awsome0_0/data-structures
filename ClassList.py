ClasslistNames = []
ClasslistScores = []
ClassListAve = []
numA = int(input('How many Students? '))
numB = int(input('How many Scores? '))
for n in range(numA):
    Names = input('Enter Name : ')
    ClasslistNames.append(Names)
    ClasslistScores.append([])
    for i in range(numB):
        Scores = int(input('Enter Score : '))
        ClasslistScores[n].append(Scores)      
    ClassListAve.append((sum(ClasslistScores[n]))/numB)
print("{0:10} {1}".format("Name", "Average"))
for x in range(numA):
    print("{0:10} {1}".format(ClasslistNames[x],ClassListAve[x]))